# Infrastructure test page.
from flask import Flask, render_template, request, redirect, url_for
from sqlalchemy.sql import text

from model import db
from model import app
from model import Task


@app.route('/hello')
def hello():
    return "Hello, World!"


@app.route("/abc", methods=["GET", "POST"])
def index1():
    mysql_result = False
    query_string = text("SELECT 1")
    # TODO REMOVE FOLLOWING LINE AFTER TESTING COMPLETE.
    db.session.query("1").from_statement(query_string).all()
    try:
        if db.session.query("1").from_statement(query_string).all():
            mysql_result = True
    except:
        pass

    if mysql_result:
        result = "Success"
    else:
        result = "Success"

    # Return the page with the result.
    return result


@app.route("/", methods=["GET", "POST"])
def index():
    all_tasks = Task.query.all()
    return render_template("index.html", tasks=all_tasks, edit_task=False)


@app.route("/add", methods=["GET", "POST"])
def add():
    if request.form:
        # print(request.form)
        task = Task(name=request.form.get("name"))
        db.session.add(task)
        db.session.commit()
        return redirect(url_for('index'))


@app.route("/edit", methods=["GET", "POST"])
def edit():
    task_id = request.args.get('task_id')
    # print("all from edit ", task_id)
    edit_task = Task.query.filter_by(id=task_id).first()
    # print(edit_task)
    all_tasks = Task.query.all()
    return render_template("index.html", tasks=all_tasks, edit_task=edit_task)


@app.route("/update", methods=["POST"])
def update():
    task_id = request.form.get("id")
    name = request.form.get("name")
    task = Task.query.filter_by(id=task_id).first()
    task.id = task_id
    task.name = name
    db.session.commit()
    return redirect(url_for('index'))


@app.route("/mark_pending", methods=["GET", "POST"])
def mark_pending():
    task_id = request.args.get('task_id')
    task = Task.query.filter_by(id=task_id).first()
    task.id = task_id
    task.is_completed = 0
    db.session.commit()
    return redirect(url_for('index'))


@app.route("/mark_done", methods=["GET", "POST"])
def mark_done():
    task_id = request.args.get('task_id')
    task = Task.query.filter_by(id=task_id).first()
    task.id = task_id
    task.is_completed = 1
    db.session.commit()
    return redirect(url_for('index'))


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=9000, debug=True)
