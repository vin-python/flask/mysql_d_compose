# Infrastructure test page.
import os
from flask import Flask
from flask import Markup
from flask import render_template
from flask_sqlalchemy import SQLAlchemy
from flask_script import Manager
from flask_migrate import Migrate, MigrateCommand


app = Flask(__name__)

# Configure MySQL connection.
db = SQLAlchemy()
db_uri = 'mysql://root:password@db/d_compose'
app.config['SQLALCHEMY_DATABASE_URI'] = db_uri
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db.init_app(app)

# Database migration command line
migrate = Migrate(app, db)
manager = Manager(app)
manager.add_command('db', MigrateCommand)


class Task(db.Model):
    # Data Model User Table
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(120), unique=False)
    is_completed = db.Column(db.Integer)

    def __init__(self, name, is_completed=0):
        # initialize columns
        self.name = name
        self.is_completed = is_completed

    def __repr__(self):
        return '<User %r>' % self.name


if __name__ == '__main__':
    manager.run()